These are a series of scripts to aid in using the LIGO remote EPICS build.

remote_epics - Creates a connection to the given site, setting up basic EPICS and CDS variables.  Then launches a shell (or other specified program)

medm_lho - launches the lho sitemap, creating a connection to the LHO EPICS gateway if needed.

medm_llo - launches the lho sitemap, creating a connection to the LLO EPICS gateway if needed.